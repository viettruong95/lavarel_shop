@extends('admin_master')
@section('title', 'Đơn hàng mới/ Chưa giải quyết xong')

@section('content')
@if (isset($error) > 0)
<div class="alert alert-danger">
	{{ $error }}
</div>
@endif
@if (isset($success) > 0)
<div class="alert alert-success">
	{{ $success }}
</div>
@endif
<a href="{{url('admin/order/list_order?status=1')}}">
	<button class="btn btn-primary" style="float: right; margin-bottom: 20px;margin-left: 20px" >
		Đơn chưa giải quyết
	</button>
</a>
<a href="{{url('admin/order/list_order?status=2')}}">
	<button class="btn btn-primary" style="float: right; margin-bottom: 20px;margin-left: 20px" >
		Đơn đang giải quyết
	</button>
</a>
<a href="{{url('admin/order/list_order?status=3')}}">
	<button class="btn btn-primary" style="float: right; margin-bottom: 20px;" >
		Đơn đã giải quyết
	</button>
</a>
<table class="table table-bordered table-striped" style="text-align: center;">
	<thead >
		<tr >
			<th style="text-align: center;">Mã đơn hàng</th>
			<th style="text-align: center;">Tên sản phẩm</th>
			<th style="text-align: center;">Số lượng</th>
			<th style="text-align: center;">Tổng giá</th>
			<th style="text-align: center;">Nhân viên phụ trách</th>
			<th style="text-align: center;">Mã nhân viên phụ trách</th>
			<th style="text-align: center;">Ngày đạt hàng</th>
			<th style="text-align: center;">Cập nhật lần cuối</th>
			<th style="text-align: center;">Trạng thái</th>
			<th></th>
			<th></th>

		</tr>
	</thead>
	<tbody>
		@foreach ($order as $or )
		@foreach ($order_detail as $detail )
		@if($detail->order_id == $or->id)
		<tr>
			<td>{{$or->id}}</td>
			<td>{{$detail->name_pro}}</td>
			<td>{{$detail->number}}</td>
			<td>{{$or->total_price}}</td>
			<td>{{$or->name_emp}}</td>
			<td>{{$or->id_emp}}</td>
			<td>{{$or->created_at}}</td>
			<td>{{$or->updated_at}}</td>
			<td>
				@if($or->status == 1)
				Chưa giải quyết
				@elseif($or->status == 2)
				Đang giải quyết
				@elseif($or->status == 3)
				Đã giải quyết
				@endif

			</td>
			<td><a href="{{url('admin/order/edit',$or->id)}}">Cập nhật</a></td>
			<td><a href="{{url('admin/handle_delete_order',$or->id)}}" onclick="return confirm('Bạn có chắc muốn xóa sản phẩm này ko')">Xóa</a></td>
		</tr>
		@endif
		@endforeach
		@endforeach

	</tbody>
</table>
{!! $order->links() !!}

@endsection