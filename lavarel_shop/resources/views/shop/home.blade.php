@extends('master')
@section('title', 'Trang chủ')

@section('content')
<div id="myCarousel" class="carousel slide" data-ride="carousel">
	<!-- Indicators -->
	<ol class="carousel-indicators">
		<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		<li data-target="#myCarousel" data-slide-to="1"></li>
		<li data-target="#myCarousel" data-slide-to="2"></li>
	</ol>

	<!-- Wrapper for slides -->
	<div class="carousel-inner">
		<div class="item active">
			<img src="http://penang.wowrentalcar.com/wp-content/uploads/2017/02/wowrentalcar_slide1_3.jpg" style="width:100%;">
		</div>

		<div class="item">
			<img src="http://penang.wowrentalcar.com/wp-content/uploads/2017/02/wowrentalcar_slide1_3.jpg" style="width:100%;">
		</div>

		<div class="item">
			<img src="http://penang.wowrentalcar.com/wp-content/uploads/2017/02/wowrentalcar_slide1_3.jpg" style="width:100%;">
		</div>
	</div>

	<!-- Left and right controls -->
	<a class="left carousel-control" href="#myCarousel" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left"></span>
		<span class="sr-only">Previous</span>
	</a>
	<a class="right carousel-control" href="#myCarousel" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right"></span>
		<span class="sr-only">Next</span>
	</a>
</div>

<div class="container-fluid" style="padding-left: 70px;">
	<div class="row">
		<div class="col-md-9">
			<h3 class="page-header"><a href="">Điện thoại</a>
					<small>Mới nhất</small>	
				</h3>
		</div>
		
	</div>


	<!-- Projects Row -->
	<div class="row">
		<div class="col-md-12">
			@foreach($list_phone as $product)
			<div class="col-md-2 portfolio-item" style="height: 360px; width: 235px;">
				<div class="listcar" style="height: 360px;  padding: 1px">
					<a href="{{url('/detail',$product->id)}}">
						<img  src=" {{$product->image}}"  height="250px" width="100%">
					</a>
					<h4 style="height: 40px; overflow: hidden;">
						<center><a href="{{url('/detail',$product->id)}}">{{ $product->name}}</a></center>
					</h4>
					<h3 style="color: #da1821"><center><strong> {{ $product->price}} VNĐ</strong></center></h3>
					
				</div>
			</div>
			@endforeach
		</div>
		
	</div>
	<!-- /.row -->
</div>

<div class="container-fluid" style="padding-left: 70px">
	<div class="row">
		<div class="col-md-9">
				<h3 class="page-header"><a href="">Máy tính sách tay</a>
					<small>Mới nhất</small>	
				</h3>
		
		</div>
		
	</div>


	<!-- Projects Row -->
	<div class="row">
		<div class="col-md-12">
			@foreach($list_laptop as $product)
			<div class="col-md-2 portfolio-item" style="height: 360px; width: 235px;">
				<div class="listcar" style="height: 360px;  padding: 1px">
					<a href="{{url('/detail',$product->id)}}">
						<img  src=" {{$product->image}}"  height="250px" width="100%">
					</a>
					<h4 style="height: 40px; overflow: hidden;">
						<center><a href="{{url('/detail',$product->id)}}">{{ $product->name}}</a></center>
					</h4>
					<h3 style="color: #da1821"><center><strong> {{ $product->price}} VNĐ</strong></center></h3>
					<!-- @if($product->status == 1)
					<p><strong>Trạng thái:</strong> Còn hàng</p>
					@elseif($product->status == 2)
					<p><strong>Trạng thái:</strong> Hết hàng</p>
					@endif -->
				</div>
			</div>
			@endforeach
		</div>
	</div>
	<!-- /.row -->
</div>
<div class="container-fluid" style="padding-left: 70px">
	<div class="row">
			<div class="col-md-9">
				<h3 class="page-header"><a href="">Máy tính bảng</a>
					<small>Mới nhất</small>	
				</h3>
		
		</div>
		
	</div>


	<!-- Projects Row -->
	<div class="row">
		<div class="col-md-12">
			@foreach($list_tablet as $product)
			<div class="col-md-2 portfolio-item" style="height: 360px; width: 235px;">
				<div class="listcar" style="height: 360px;  padding: 1px">
					<a href="{{url('/detail',$product->id)}}">
						<img  src=" {{$product->image}}"  height="250px" width="100%">
					</a>
					<h4 style="height: 40px; overflow: hidden;">
						<center><a href="{{url('/detail',$product->id)}}">{{ $product->name}}</a></center>
					</h4>
					<h3 style="color: #da1821"><center><strong> {{ $product->price}} VNĐ</strong></center></h3>
					<!-- @if($product->status == 1)
					<p><strong>Trạng thái:</strong> Còn hàng</p>
					@elseif($product->status == 2)
					<p><strong>Trạng thái:</strong> Hết hàng</p>
					@endif -->
				</div>
			</div>
			@endforeach
		</div>
	</div>
	<!-- /.row -->
</div>
<div class="container-fluid" style="padding-left: 70px">
	<div class="row">
			<div class="col-md-9">
				<h3 class="page-header"><a href="">Phụ kiện</a>
					<small>Mới nhất</small>	
				</h3>
		
		</div>
		
	</div>


	<!-- Projects Row -->
	<div class="row">
		<div class="col-md-12">
			@foreach($list_sub as $product)
			<div class="col-md-2 portfolio-item" style="height: 360px; width: 235px;">
				<div class="listcar" style="height: 360px;  padding: 1px">
					<a href="{{url('/detail',$product->id)}}">
						<img  src=" {{$product->image}}"  height="250px" width="100%">
					</a>
					<h4 style="height: 40px; overflow: hidden;">
						<center><a href="{{url('/detail',$product->id)}}">{{ $product->name}}</a></center>
					</h4>
					<h3 style="color: #da1821"><center><strong> {{ $product->price}} VNĐ</strong></center></h3>
					<!-- @if($product->status == 1)
					<p><strong>Trạng thái:</strong> Còn hàng</p>
					@elseif($product->status == 2)
					<p><strong>Trạng thái:</strong> Hết hàng</p>
					@endif -->
				</div>
			</div>
			@endforeach
		</div>
		
	</div>
	<!-- /.row -->
</div>




@endsection