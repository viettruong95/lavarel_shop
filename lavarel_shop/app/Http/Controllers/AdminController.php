<?php

namespace App\Http\Controllers;
use Session;
use Illuminate\Http\Request;
use App\User;
use App\Account;
use App\Product;
use App\Trade;
use App\Image;
use App\Color;
use App\Order;
use App\Order_Detail;
use App\Pro_color;
use App\Description;

class AdminController extends Controller
{

	public function home()
	{

		if (session::has('user_name')){
			
			return view('admin.home');
		}else{
			return  redirect('admin/login');
		}

	}

	public function login()
	{

		return view('admin.login');
	}  
	public function handle_logout()
	{
		session::flush();
		return  redirect('admin/login');
	}  

	public function handle_login(Request $request)
	{
		$acc = Account::where('user_name', $request->user_name)
		->where('password',md5($request->password))
		->where(function($q) {
			$q->where('type', 1)
			->orWhere('type', 2);
		})
		->first();

		if($acc){
			$user = Account::find($acc->id)->user;
			session::put('id', $acc->id);
			session::put('user_name', $acc->user_name);
			session::put('image', $acc->image);
			session::put('name', $user->name);
			session::put('type', $user->type);
			return  redirect('admin/home');
		}else{
			$errors = 'Sai tên tài khoản hoặc mật khẩu';
			return  redirect('admin/login')->withErrors($errors);

		}
	}

	public function list_trade(Request $request){
		if (session::has('user_name')){
			if(($request->get('id')) > 0){
				$trade = Trade::find($request->get('id'));
				if( $trade){
					$list_trade = Trade::all();
					return view('admin.mn_System.trade',['list_trade' => $list_trade, 'trade' => $trade]);
				}else{
					$list_trade = Trade::all();
					return view('admin.mn_System.trade',['list_trade' => $list_trade, 'error' => 'Thao tác thất bại! Không tồn tại hãng này']);
				}
			}
			$list_trade = Trade::all();
			return view('admin.mn_System.trade',['list_trade' => $list_trade]);
		}else{
			return  redirect('admin/login');

		}

	}
	public function trade_handling(Request $request){
		if (session::has('user_name')){

			if($request->txtId == 0){
                //add
				$check = Trade::where('name', $request->txtName)->first();
                //check xem name đã tồn tại chưa
				if($check){
                //đã tồn tại => ko cho thêm => suất ra lỗi
					$list_trade = Trade::all();
					return view('admin.mn_System.trade',['list_trade' => $list_trade, 'error' => 'Thao tác thất bại! Hãng sản xuất này đã tồn tại']);
				}else{
                // chưa tồn tại => cho phép thêm mới
					$trade = new Trade;
					$trade->name = $request->txtName;
					$trade->save();
				}

			}else {
                //edit
				$trade = Trade::where('id', $request->txtId)->first();
                //check xem id có tồn tại ko
				if($trade){
                // tồn tại => cho phép sửa
					$trade->name = $request->txtName;
					$trade->save();
				}else{
                // không tồn tại => ko có sửa => báo ra lỗi
					$list_trade = Trade::all();
					return view('admin.mn_System.trade',['list_trade' => $list_trade, 'error' => 'Thao tác không thành công! Không thế sửa hãng sản xuất chưa tồn tại']);
				}
			}

			$list_trade = Trade::all();
			return view('admin.mn_System.trade',['list_trade' => $list_trade, 'success' => 'Thao tác thành công']);
		}else{
			return  redirect('admin/login');

		}

	}





	public function list_color(Request $request){
		if (session::has('user_name')){
			if(($request->get('id')) > 0){
				$color = Color::find($request->get('id'));
				if( $color){
					$list_color = Color::all();
					return view('admin.mn_System.color',['list_color' => $list_color, 'color' => $color]);
				}else{
					$list_color = Color::all();
					return view('admin.mn_System.color',['list_color' => $list_color, 'error' => 'Thao tác thất bại! Không tồn tại màu này']);
				}
			}
			$list_color = Color::all();
			return view('admin.mn_System.color',['list_color' => $list_color]);
		}else{
			return  redirect('admin/login');

		}

	}
	public function color_handling(Request $request){
		if (session::has('user_name')){

			if($request->txtId == 0){
                //add
				$check = Color::where('name', $request->txtName)->first();
                //check xem name đã tồn tại chưa
				if($check){
                //đã tồn tại => ko cho thêm => suất ra lỗi
					$list_color = Color::all();
					return view('admin.mn_System.color',['list_color' => $list_color, 'error' => 'Thao tác thất bại! Màu sắc này đã tồn tại']);
				}else{
                // chưa tồn tại => cho phép thêm mới
					$color = new Color;
					$color->name = $request->txtName;
					$color->save();
				}

			}else {
                //edit
				$color = Color::where('id', $request->txtId)->first();
                //check xem id có tồn tại ko
				if($color){
                // tồn tại => cho phép sửa
					$color->name = $request->txtName;
					$color->save();
				}else{
                // không tồn tại => ko có sửa => báo ra lỗi
					$list_color = Color::all();
					return view('admin.mn_System.color',['list_color' => $list_color, 'error' => 'Thao tác không thành công! Không thế sửa màu sắc chưa tồn tại']);
				}
			}

			$list_color = Color::all();
			return view('admin.mn_System.color',['list_color' => $list_color, 'success' => 'Thao tác thành công']);
		}else{
			return  redirect('admin/login');

		}

	}
	public function list_product(){
		if (session::has('user_name')){
			$list_product = Product::paginate(10);
			$list_trade = Trade::all();
			
			return view('admin.mn_product.list_product',['list_product' => $list_product, 'list_trade' => $list_trade]);
		}else{
			return  redirect('admin/login');

		}
	}
	public function handle_delete_pro($id_pro)
	{
		if (session::has('user_name')){
			Product::find($id_pro)->delete();

			$list_product = Product::paginate(10);
			$list_trade = Trade::all();
			return redirect('admin/product');
		}else{
			return  redirect('admin/login');

		}

	} 
	public function add_product()
	{
		if (session::has('user_name')){
			$list_trade = Trade::all();
			$list_type = array(
				array('id'=>'1', 'name'=>'Điện thoại'),
				array('id'=>'2', 'name'=>'Máy tính xách tay'),
				array('id'=>'3', 'name'=>'Máy tính bảng'),
				array('id'=>'4', 'name'=>'Phụ kiện')

			);
			return view('admin.mn_product.add_product',['list_trade' => $list_trade, 'list_type' => $list_type]);
		}else{
			return  redirect('admin/login');

		}

	}
	public function handle_add_pro(Request $request)
	{
		if (session::has('user_name')){
			$des = new Description;
			$product = new Product;
			$product->name = $request->name;
			$product->price = $request->price;
			$product->trade_id = $request->trade;
			$product->type_id = $request->type;
			$product->image = $request->default_image;
			$product->status= $request->status;
			if($request->hasFile('image')){
				$file = $request->image;
				$product->image = $file->move('image',$file->getClientOriginalName());

			}

			$des->size = $request->size;
			$des->resolution = $request->resolution;
			$des->operating = $request->operating;
			$des->ram = $request->ram;
			$des->cpu = $request->cpu;
			$des->camera = $request->camera;
			$des->memory = $request->memory;
			$des->pin = $request->pin;
			$des->save();
			$product->description = $des->id;
			$product->save();
			$list_trade = trade::all();
			return view('admin.mn_product.add_product',['list_trade' => $list_trade,'success' => 'Thao tác thành công']);

		}else{
			return  redirect('admin/login');

		}

	}
	public function edit_product($id){
		$pro = Product::find($id);
		$des = Product::find($id)->description()->first();
		$list_trade = Trade::all();
		if (session::has('user_name')){
			return view('admin.mn_product.edit_product', ['list_trade' => $list_trade,'pro' => $pro, 'des' => $des]);
		}else{
			return  redirect('admin/login');
			
		}
	}
	public function handle_edit_pro(Request $request)
	{
		if (session::has('user_name')){
			$des = Description::find($request->des_id);
			$des->size = $request->size;
			$des->resolution = $request->resolution;
			$des->operating = $request->operating;
			$des->ram = $request->ram;
			$des->cpu = $request->cpu;
			$des->camera = $request->camera;
			$des->memory = $request->memory;
			$des->pin = $request->pin;
			$des->save();
			$product = Product::find($request->id);
			$product->name = $request->name;
			$product->price = $request->price;
			$product->trade_id = $request->trade;
			$product->image = $product->image;
			$product->type_id = $request->type;
			$product->status= $request->status;
			if($request->hasFile('image')){
				$file = $request->image;
				$product->image = $file->move('image',$file->getClientOriginalName());
			}
			$product->save();
			return redirect('admin/product');

		}else{
			return  redirect('admin/login');

		}

	}
	public function list_acc(Request $request){
		if (session::has('user_name') & session::has('type')==1){
			if(($request->get('type')) > 0){
				
				$list_acc = Account::where('type',$request->get('type'))->paginate(10);
				return view('admin.mn_account.list_acc',['list_acc' => $list_acc]);
				
			}
			return redirect('admin/acc');

		}else{
			return  redirect('admin/login');

		}
	}
	public function add_acc(Request $request){
		if (session::has('user_name') & session::has('type')==1){
			return view('admin.mn_account.add_acc');
		}else{
			return  redirect('admin/login');

		}
	}
	public function handle_add_acc(Request $request){
		if (session::has('user_name') & session::has('type')==1){
				//add
			$check = Account::where('user_name', $request->user_name)->first();
                //check xem name đã tồn tại chưa
			if($check){
                //đã tồn tại => ko cho thêm => suất ra lỗi
				return view('admin.mn_account.add_acc',['error' => 'Thao tác thất bại! Tên đăng nhập này đã tồn tại']);
			}else{
				if($request->password != $request->re_password){
					return view('admin.mn_account.add_acc',['error' => 'Thao tác thất bại! Mật khẩu và mật khẩu nhập lại không giống nhau']);
				}else{
					$user = new User;
					$user->name = $request->name;
					$user->email = $request->email;
					$user->address = $request->address;
					$user->phone = $request->phone;
					$user->type = $request->type;

					$acc = new Account;
					$acc->user_name = $request->user_name;
					$acc->password = MD5($request->password);
					$acc->image = $request->default_image;
					$acc->type= $request->type;
					if($request->hasFile('image')){
						$file = $request->image;
						$acc->image = $file->move('public/image',$file->getClientOriginalName());
					}
					$user->save();
					$acc->id_user= $user->id;
					$acc->save();

				}
			} 
                // chưa tồn tại => cho phép thêm mới
			return view('admin.mn_account.add_acc',['success' => 'Thao tác thành công']);
		}else{
			return  redirect('admin/login');

		}
	}
	public function list_order(Request $request){
		if (session::has('user_name')){
			if(($request->get('status')) == 1){
				$order = Order::where('status', 1)->orderBy('id', 'desc')->paginate(10);
			}
			else if(($request->get('status')) == 2){
				$order = Order::where('status', 2)->orderBy('id', 'desc')->paginate(10);
			}
			else if(($request->get('status')) == 3){
				$order = Order::where('status', 3)->orderBy('id', 'desc')->paginate(10);
			}
			else{
				$order = Order::orderBy('id', 'desc')->paginate(10);
			}
			$order_detail = Order_Detail::all();
			return view('admin.mn_order.list_order',['order' => $order, 'order_detail' => $order_detail]);
		}else{
			return  redirect('admin/login');

		}
	}
	public function edit_order($id){
		if (session::has('user_name')){
			$order = Order::find($id);
			$order_detail = Order::find($id)->order_detail()->first();

			return view('admin.mn_order.edit_order',['order' => $order, 'order_detail' => $order_detail]);
		}else{
			return  redirect('admin/login');

		}
	}	
	public function handle_order(Request $request){
		if (session::has('user_name')){
			$order = Order::find($request->id);
			$order->status =  $request->status;
			$order->name_emp =  session::get('name');
			$order->id_emp =  session::get('id');
			$order->save();
			$order_detail = Order::find($request->id)->order_detail()->first();
			return view('admin.mn_order.edit_order',['order' => $order, 'order_detail' => $order_detail, 'success' => 'Thao tác thành công']);
		}else{
			return  redirect('admin/login');

		}
	}



}
