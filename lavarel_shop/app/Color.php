<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
   protected $table = 'color';
	protected $fillable = ['id','name'];
	public $timestamps = false;

	public function pro_color (){
		return $this->belongsToMany('App\Pro_color', 'pro_color','color_id');
	}
}
