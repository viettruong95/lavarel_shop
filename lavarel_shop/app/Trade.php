<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trade extends Model
{
    protected $table = 'trade';
	protected $fillable = ['id','name'];
	public $timestamps = false;

	public function product (){
		 return $this->hasMany('App\Product', 'trade_id');
	}
}
