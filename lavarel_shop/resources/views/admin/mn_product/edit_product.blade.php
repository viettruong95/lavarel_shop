@extends('admin_master')
@section('title', 'Sửa sản phẩm')

@section('content')
@if (isset($error) > 0)
<div class="alert alert-danger">
	{{ $error }}
</div>
@endif
@if (isset($success) > 0)
<div class="alert alert-success">
	{{ $success }}
</div>
@endif
<?php
$list_type = array(
	array('id'=>'1', 'name'=>'Điện thoại'),
	array('id'=>'2', 'name'=>'Máy tính xách tay'),
	array('id'=>'3', 'name'=>'Máy tính bảng'),
	array('id'=>'4', 'name'=>'Phụ kiện')

);?>
<form class="form-horizontal" action="{{ url('/admin/handle_edit_pro') }}" enctype="multipart/form-data" method="POST">
	{{ csrf_field()}}
	<div class="col-md-6" style="border-right: 1px solid silver">
		<div class="form-group">
			<label class="control-label  col-sm-4" >Tên sản phẩm:</label>
			<div class="col-sm-7">
					<input type="hidden" class="form-control" name="id" required value="{{$pro->id}}">
					<input type="hidden" class="form-control" name="des_id" required value="{{$des->id}}">

				<input type="text" class="form-control" name="name" placeholder="Nhập tên sản phẩm" required value="{{$pro->name}}">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-sm-4" >Giá sản phẩm:</label>
			<div class="col-sm-7">
				<input type="text" class="form-control" name="price" placeholder="Nhập giá của sản phẩm" value="{{$pro->price}}">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4" >Ảnh:</label>
			<div class="col-sm-7">
				<input type="file" class="form-control" id="chooseimg" name="image" value="{{$pro->image}}" >
				<img id="image" height="300px" width="300px" style="margin-top: 20px" src="{{asset($pro->image)}}" />
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4" >Trạng thái:</label>
			<div class="col-sm-7">
				<select class="form-control"  name="status">
					@if($pro->status == 1)
					<option value="1" selected>Còn hàng</option>
					<option value="2">Hết hàng</option>
					@else
					<option value="1">Còn hàng</option>
					<option value="2" selected>Hết hàng</option>
					@endif

				</select>
			</div>
		</div>
	</div>

	<div class="col-md-6" >
		
		<div class="form-group">
			<label class="control-label col-sm-4" >Hãng sản xuất:</label>
			<div class="col-sm-7">
				<select class="form-control"  name="trade">
					@foreach($list_trade as $trade)
					@if($pro->trade_id == $trade->id)
					<option value="{{$trade->id}}" selected>{{$trade->name}}</option>
					@else
					<option value="{{$trade->id}}">{{$trade->name}}</option>
					@endif
					@endforeach
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4" >Loại sản phẩm:</label>
			<div class="col-sm-7">
				<select class="form-control"  name="type">
					@foreach($list_type as $type)
					@if($pro->trade_id == $trade->id)
					<option value="{{$type['id']}}" selected>{{$type['name']}}</option>
								@else
					<option value="{{$type['id']}}">{{$type['name']}}</option>

					@endif

					@endforeach
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label  col-sm-4" >Kích thước màn hình:</label>
			<div class="col-sm-7">
				<input type="text" class="form-control" name="size" placeholder="Nhập kích thước màn hình" required value="{{$des->size}}">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label  col-sm-4" >Độ phân giải:</label>
			<div class="col-sm-7">
				<input type="text" class="form-control" name="resolution" placeholder="Nhập độ phân giải" required value="{{$des->resolution}}">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label  col-sm-4" >Hệ điều hành:</label>
			<div class="col-sm-7">
				<input type="text" class="form-control" name="operating" placeholder="Nhập hệ điều hành" required value="{{$des->operating}}">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label  col-sm-4" >Chip xử lý(CPU):</label>
			<div class="col-sm-7">
				<input type="text" class="form-control" name="cpu" placeholder="Nhập Chip xử lý(CPU)" required value="{{$des->cpu}}">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label  col-sm-4" >RAM:</label>
			<div class="col-sm-7">
				<input type="text" class="form-control" name="ram" placeholder="Nhập RAM" required value="{{$des->ram}}">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label  col-sm-4" >Máy ảnh chính:</label>
			<div class="col-sm-7">
				<input type="text" class="form-control" name="camera" placeholder="Nhập máy ảnh chính" required value="{{$des->camera}}">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label  col-sm-4" >Bộ nhớ trong:</label>
			<div class="col-sm-7">
				<input type="text" class="form-control" name="memory" placeholder="Nhập bộ nhớ trong" required value="{{$des->memory}}">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label  col-sm-4" >Pin:</label>
			<div class="col-sm-7">
				<input type="text" class="form-control" name="pin" placeholder="Nhập pin" required value="{{$des->pin}}">
			</div>
		</div>
		<input class="btn btn-primary col-sm-offset-4" type="submit" value="Sửa">
		<a href="{{url('admin/product')}}"><input class="btn btn-danger" value="Hủy" style="width: 14%"></a>
		
	</div>
	
</form>
<script type="text/javascript">
	var file = document.getElementById('chooseimg');
	var img = document.getElementById('image');
	file.addEventListener("change", function() {
		if (this.value) {
			var file = this.files[0];
			var reader = new FileReader();
			reader.onloadend = function () {
				img.src = reader.result;
				
			};
			reader.readAsDataURL(file);
		}
	});
</script>
@endsection
