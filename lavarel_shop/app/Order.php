<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
   	// status: 1 - mới tạo chưa giải quyết
   	// 		2 - đang giải quyết
   	// 		3 - đã giải quyết
	protected $table = 'order';
	protected $fillable = ['id','name_cus','name_emp','total_price','address_cus','phone_cus','email_cus','id_emp','status','id_cus'];

	public function order_detail (){
		 return $this->hasMany('App\Order_Detail', 'order_id');
	}

}
