<?php
$list_type = array(
	array('id'=>'1', 'name'=>'Điện thoại'),
	array('id'=>'2', 'name'=>'Máy tính xách tay'),
	array('id'=>'3', 'name'=>'Máy tính bảng'),
	array('id'=>'4', 'name'=>'Phụ kiện')

);
?>
@extends('admin_master')
@section('title', 'Quản lý sản phẩm')

@section('content')
@if (isset($error) > 0)
<div class="alert alert-danger">
	{{ $error }}
</div>
@endif
@if (isset($success) > 0)
<div class="alert alert-success">
	{{ $success }}
</div>
@endif

<a href="{{url('admin/product/add')}}"><button class="btn btn-primary" style="float: right; margin-bottom: 20px" >Thêm mới sản phẩm</button></a>
<table class="table table-bordered table-striped" style="text-align: center;">
	<thead >
		<tr >
			<th style="text-align: center;">Mã sản phẩm</th>
			<th style="text-align: center;">Ảnh</th>
			<th style="text-align: center;">Tên sản phẩm</th>
			<th style="text-align: center;">Giá</th>
			<th style="text-align: center;">Hãng sản xuất</th>
			<th style="text-align: center;">Loại sản phẩm</th>
			<th style="text-align: center;">Trạng thái</th>
			<th></th>
			<th></th>

		</tr>
	</thead>
	<tbody>
		@foreach ($list_product as $pro )
		<tr>
			<td>{{$pro->id}}</td>
			<td><img src="/{{$pro->image}}" style="width: 100px; height: 100px"></td>
			<td>{{$pro->name}}</td>
			<td>{{$pro->price}}</td>
			<td>
				@foreach ($list_trade as $trade ) 
				@if( $trade->id == $pro->trade_id)
				{{$trade->name}}
				@endif
				@endforeach
			</td>
			<td>
				@foreach ($list_type as $type ) 
				@if( $type['id'] == $pro->type_id)
				{{$type['name']}}
				@endif
				@endforeach
			</td>
			<td>
				@if( $pro->status == 1)
				Còn hàng
				@elseif( $pro->status == 2)
				Hết hàng
				@endif
			</td>
			<td><a href="{{url('admin/product/edit',$pro->id)}}">Sửa</a></td>
			<td><a href="{{url('admin/handle_delete_pro',$pro->id)}}" onclick="return confirm('Bạn có chắc muốn xóa sản phẩm này ko')">Xóa</a></td>
		</tr>
		
		@endforeach

	</tbody>
</table>
{!! $list_product->links() !!}
@endsection
