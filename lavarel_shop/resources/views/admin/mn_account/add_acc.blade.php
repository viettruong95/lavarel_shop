@extends('admin_master')
@section('title', 'Thêm mới sản phẩm')

@section('content')
@if (isset($error) > 0)
<div class="alert alert-danger">
	{{ $error }}
</div>
@endif
@if (isset($success) > 0)
<div class="alert alert-success">
	{{ $success }}
</div>
@endif

<form class="form-horizontal" action="{{ url('/admin/handle_add_acc') }}" enctype="multipart/form-data" method="POST">
	{{ csrf_field()}}
	<div class="form-group">
		<label class="control-label col-sm-offset-2 col-sm-2" >Tên dăng nhập:</label>
		<div class="col-sm-4">
			<input type="text" class="form-control" name="user_name" placeholder="Nhập tên đăng nhập" required >
		</div>
	</div>
	
	<div class="form-group">
		<label class="control-label col-sm-offset-2 col-sm-2" >Loại tài khoản:</label>
		<div class="col-sm-4">
			<select class="form-control"  name="type">
				<option value="1">Admin</option>
				<option value="2">Nhân viên</option>
				<option value="3">Khách hàng</option>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-offset-2 col-sm-2" >Mật khẩu:</label>
		<div class="col-sm-4">
			<input type="password" class="form-control" name="password" placeholder="Nhập mật khẩu" required>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-offset-2 col-sm-2" >Nhập lại mật khẩu:</label>
		<div class="col-sm-4">
			<input type="password" class="form-control" name="re_password" placeholder="Nhập lại mật khẩu" required>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-offset-2 col-sm-2" >Tên mẹ đạt cho:</label>
		<div class="col-sm-4">
			<input type="text" class="form-control" name="name" placeholder="Nhập tên mẹ đạt cho" required >
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-offset-2 col-sm-2" >Số điện thoại:</label>
		<div class="col-sm-4">
			<input type="text" class="form-control" name="phone" placeholder="Nhập số điện thoại" required >
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-offset-2 col-sm-2" >Email:</label>
		<div class="col-sm-4">
			<input type="email" class="form-control" name="email" placeholder="Nhập email" required >
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-offset-2 col-sm-2" >Địa chỉ:</label>
		<div class="col-sm-4">
			<input type="text" class="form-control" name="address" placeholder="Nhập địa chỉ" required >
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-offset-2 col-sm-2" >Ảnh:</label>
		<div class="col-sm-4">
			<input type="file" class="form-control" id="chooseimg" name="image"  >
			<input type="hidden" name="default_image" value="image/default-image_900.png">
			<img id="image" height="350px" width="350px" style="margin-top: 20px" src="/image/default-image_900.png" />
		</div>
	</div>
	
	<input class="btn btn-primary col-sm-offset-4" type="submit" value="Thêm mới">
	<a href="{{url('admin/home')}}"><input class="btn btn-danger" value="Hủy" style="width: 7%"></a>
</form>
<script type="text/javascript">
	var file = document.getElementById('chooseimg');
	var img = document.getElementById('image');
	file.addEventListener("change", function() {
		if (this.value) {
			var file = this.files[0];
			var reader = new FileReader();
			reader.onloadend = function () {
				img.src = reader.result;
				
			};
			reader.readAsDataURL(file);
		}
	});
</script>




@endsection
