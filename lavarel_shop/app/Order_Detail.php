<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order_Detail extends Model
{
    protected $table = 'order_detail';
	protected $fillable = ['id','name_pro','price','number','order_id','product_id'];
	public $timestamps = false;

	public function order()
    {
        return $this->belongsTo('App\Order', 'order_id');
    }
}
