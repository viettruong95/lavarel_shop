<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pro_color extends Model
{
	protected $table = 'pro_color';
	protected $fillable = ['id','product_id','color_id'];

	public function product (){
		return $this->belongsToMany('App\Product', 'product','product_id');
	}
	public function color (){
		return $this->belongsToMany('App\Color', 'color','color_id');
	}
}
