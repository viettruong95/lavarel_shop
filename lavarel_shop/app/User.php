<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{

	// type: 1 - admin, 2 - employee, 3 - customer
	protected $table = 'user';
	protected $fillable = ['id','name','phone','email','address','type'];
	public $timestamps = false;

	public function account(){
		return $this->hasOne('App\Account', 'id_user');
	}
}
