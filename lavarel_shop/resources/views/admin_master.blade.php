<!DOCTYPE html>
<html>
<head>
	<title>Admin - @yield('title')</title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc= sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
	<link rel="stylesheet"  href="{{URL::asset('css/style_admin.css')}}">
</head>
<body>
	<header class="container-fluid">
		<div class="row">
			<nav class="navbar">
				<div class="container-fluid">
					<div class="navbar-header">
						<a class="navbar-brand" href="#">Master Shop</a>
					</div>
					<!-- <ul class="nav navbar-nav">
						<li class="active"><a href="#">Home</a></li>
						<li><a href="#">Page 1</a></li>
						<li><a href="#">Page 2</a></li>
					</ul> -->
					<form class="navbar-form navbar-left" action="/action_page.php">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="Master tìm kiếm">
						</div>
						<button type="submit" class="btn btn-default">Tìm kiếm</button>
					</form>
					<ul class="nav navbar-nav" style="float: right;">

						<li><a href="#">Xin chào {{ session::get('name')}}</a></li>
						<li><a href="{{url('/admin/handle_logout')}}">Đăng xuất</a></li>
					</ul>
					
				</div> 
			</nav>
		</div>
		
	</header>

	<div class="row">
		<div class="col-md-2 menu-left ">
			<center><img src="{{asset(session::get('image'))}}"  width="150" height="150" style=" border-radius: 50%;"><h3>{{ session::get('name')}}</h3></center>


			<ul>
				<li class="main-li"><a href="{{url('/admin/home')}}"><h4 ><span class="fa fa-dashboard"></span>Bảng điều khiển</h4></a></li>
				<li class="main-li"><a href="{{url('/admin/product')}}"><h4 ><span class="fa fa-dropbox"></span>Quản lý sản phẩm</h4></a></li>
				
				@if(session::get('type') == 1)
				<!-- Type account = 1 => admin moi dc quan ly tai khoan -->
				<li class="main-li" onclick="open_mnAcccont()"><h4 ><span class="	fa fa-user"></span>Quản lý tài khoản</h4></li>
				<div id="mnAcccont">
					<li ><a href="{{url('/admin/acc?type=1')}}"><h4 >Tài khoản admin</h4></a></li>
					<li ><a href="{{url('/admin/acc?type=2')}}"><h4 >Tài khoản nhân viên</h4></a></li>
					<li ><a href="{{url('/admin/acc?type=3')}}"><h4 >Tài khoản khách hàng</h4></a></li>
				</div>
				@endif
				<li class="main-li" onclick="open_mnOrder()"><h4 ><span class="	fa fa-book"></span>Quản lý đơn hàng</h4></li>
				<div id="mnOrder">
					<li ><a href="{{url('/admin/order/list_order')}}"><h4 >Danh sách đơn hàng</h4></a></li>

				</div>
				<li class="main-li"  onclick="open_mnEmployee()"><h4 ><span class="	fa fa-users"></span>Quản lý nhân viên</h4></li>
				<div id="mnEmployee">
					<li ><a href="#"><h4 >Chức năng đang phát triển</h4></a></li>
					<li ><a href="#"><h4 >Chức năng đang phát triển</h4></a></li>
				</div>
				<li class="main-li"  onclick="open_mnList()"><h4 ><span class="	fa fa-list"></span>Thông số hệ thống</h4></li>
				<div id="mnList">
					<li ><a href="{{url('/admin/system/trade')}}"><h4 >Hãng sản xuất</h4></a></li>

					<li ><a href="{{url('/admin/system/color')}}"><h4 >Màu sắc sản phẩm</h4></a></li>
				</div>
			</ul>
		</div>
		<div class="container-fluid">
			<div class="content col-md-10 " >
				<div class="content-title col-md-12">
					
					<h3 >@yield('title')</h3>
				</div>
				<div class="content-of-content col-md-12">
					<div class="row">
					@yield('content')
						
					</div>

				</div>
			</div>
			
		</div>
		
		
	</div>
	


	<script type="text/javascript">
		var mnAcccont = 0;
		var mnOrder = 0;
		var mnEmployee = 0;
		var mnList = 0;
		
		function open_mnAcccont(){
			mnAcccont++;
			if(mnAcccont % 2 == 0){
				document.getElementById("mnAcccont").style.display = "none";
			}else{
				document.getElementById("mnAcccont").style.display = "block";

			}

		}
		function open_mnOrder(){
			mnOrder++;
			if(mnOrder % 2 == 0){
				document.getElementById("mnOrder").style.display = "none";
			}else{
				document.getElementById("mnOrder").style.display = "block";

			}

		}
		function open_mnEmployee(){
			mnEmployee++;
			if(mnEmployee % 2 == 0){
				document.getElementById("mnEmployee").style.display = "none";
			}else{
				document.getElementById("mnEmployee").style.display = "block";

			}

		}	function open_mnList(){
			mnList++;
			if(mnList % 2 == 0){
				document.getElementById("mnList").style.display = "none";
			}else{
				document.getElementById("mnList").style.display = "block";

			}

		}
	</script>



</body>
</html>