@extends('admin_master')
@section('title', 'Quản lý hãng sản xuất')

@section('content')
<div class="col-md-6">
	@if (isset($error) > 0)
	<div class="alert alert-danger">
		{{ $error }}
	</div>
	@endif
	@if (isset($success) > 0)
	<div class="alert alert-success">
		{{ $success }}
	</div>
	@endif
	<div class="panel panel-default">
		@if(isset($_GET['id']) > 0)
		<div class="panel-heading">Sửa hãng sản xuất</div>
		<div class="panel-body">
			<form method="post" class="form-horizontal"  action="{{ url('/admin/system/trade') }}">
				{{ csrf_field()}}

				<div class="form-group">
					<label class="col-md-4 control-label">Tên hãng sản xuất</label>
					<div class="col-md-6">
						<input type="text" name="txtName" class=" form-control" required value="@if(isset($trade)){{$trade->name}}@endif">
					</div>
				</div>
				<input type="hidden" name="txtId" value="{{$_GET['id']}}">
				<div class="form-group">        
					
					
					<div class="col-md-offset-4 col-md-3 ">
						<button type="submit" class="btn btn-primary" style="width: 100%">Sửa</button>
					</div>
					<div class="col-md-3 ">
						<a href="/admin/system/trade" class="btn btn-danger" style="width: 100%">Hủy</a>
					</div>
				</div>
			</form>
		</div>


		@else
		<div class="panel-heading">Thêm mới hãng sản xuất</div>
		<div class="panel-body">
			<form method="post" class="form-horizontal"  action="{{ url('/admin/system/trade') }}">
				{{ csrf_field()}}
				<div class="form-group">
					<label class="col-md-4 control-label">Tên hãng sản xuất</label>
					<div class="col-md-6">
						<input type="text" name="txtName" class=" form-control" required >
					</div>
				</div>
				<input type="hidden" name="txtId" value="0">
				<div class="form-group">        
					<div class="col-md-offset-7 col-md-3 ">
						<button type="submit" class="btn btn-primary" style="width: 100%">Thêm mới</button>
					</div>
				</div>
			</form>
		</div>
		@endif
		
	</div>
</div>
<div class="col-md-4" style="float: right;">
	<div class="panel panel-default">
		<div class="panel-heading">Các hãng sản xuất</div>
		<div class="panel-body l_table">
			<table class="table table-bordered list_table">
				@foreach ($list_trade as $trade )
				<tr >
					<td><span><a href="trade?id={{$trade->id}}">{{$trade->name}}</a></span></td>
				</tr>

				@endforeach
				


				
				
			</table>
		</div>
	</div>
</div>

@endsection
