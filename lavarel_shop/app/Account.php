<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
	// type: 1 - admin, 2 - employee
	
	protected $table = 'account';
	protected $fillable = ['id','user_name','id_user','type','image'];
	protected $hidden = ['password'];
	public $timestamps = false;

	public function user (){
		return $this->belongsTo('App\User', 'id_user');
	}
}
