<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $table = 'product';
	protected $fillable = ['id','name','image','description','price','trade_id','type_id','status'];
	public function trade()
	{
		return $this->belongsTo('App\Trade', 'trade_id');
	}

	public function description (){
		return $this->belongsTo('App\Description', 'description');
	}
}
