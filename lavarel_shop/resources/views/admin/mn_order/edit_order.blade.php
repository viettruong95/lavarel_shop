@extends('admin_master')
@section('title', 'Đơn hàng mới/ Chưa giải quyết xong')

@section('content')
@if (isset($error) > 0)
<div class="alert alert-danger">
	{{ $error }}
</div>
@endif
@if (isset($success) > 0)
<div class="alert alert-success">
	{{ $success }}
</div>
@endif
<form class="form-horizontal" action="{{ url('/admin/order/handle_order') }}" enctype="multipart/form-data" method="POST">
	{{ csrf_field()}}
	<div class="col-md-10" >
		<div class="form-group">
			<label class="control-label  col-sm-4" >Tên sản phẩm:</label>
			<div class="col-sm-7">
				<input type="hidden" name="id" value="{{$order->id}}">
				<input type="text" class="form-control" name="name_pro" value="{{$order_detail->name_pro}}" required readonly>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-sm-4" >Giá sản phẩm:</label>
			<div class="col-sm-7">
				<input type="text" class="form-control" name="price" value="{{$order_detail->price}}" readonly>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label  col-sm-4" >Số lượng:</label>
			<div class="col-sm-7">
				<input type="text" class="form-control" name="operating" value="{{$order_detail->number}}" required readonly>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label  col-sm-4" >Tổng giá:</label>
			<div class="col-sm-7">
				<input type="text" class="form-control" name="cpu" value="{{$order->total_price}}" required readonly>
			</div>
		</div>
		
		

		
		<div class="form-group">
			<label class="control-label col-sm-4" >Tên khách hàng:</label>
			<div class="col-sm-7">
				<input type="text" class="form-control" name="size" value="{{$order->name_cus}}" required readonly>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4" >Số điện thoại:</label>
			<div class="col-sm-7">
				<input type="text" class="form-control" name="size" value="{{$order->phone_cus}}" required readonly>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label  col-sm-4" >Email:</label>
			<div class="col-sm-7">
				<input type="text" class="form-control" name="size" value="{{$order->email_cus}}" required readonly>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label  col-sm-4" >Địa chỉ:</label>
			<div class="col-sm-7">
				<input type="text" class="form-control" name="resolution" value="{{$order->address_cus}}" required readonly>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4" >Trạng thái:</label>
			<div class="col-sm-7">
				<select class="form-control"  name="status">
					@if($order->status == 1)
					<option value="1" selected>Chưa giải quyết</option>
					<option value="2">Đang giải quyết</option>
					<option value="3">Đã giải quyết</option>
					@elseif($order->status == 2)
					<option value="1" >Chưa giải quyết</option>
					<option value="2" selected>Đang giải quyết</option>
					<option value="3">Đã giải quyết</option>
					@elseif($order->status == 3)
					<option value="1">Chưa giải quyết</option>
					<option value="2">Đang giải quyết</option>
					<option value="3" selected>Đã giải quyết</option>
					@endif
				</select>
			</div>
		</div>

		
		<input class="btn btn-primary col-sm-offset-4" type="submit" value="Cập nhật">
		<a href="{{url('/admin/order/list_order')}}"><input class="btn btn-danger" value="Hủy" style="width: 14%"></a>
		
	</div>
	
</form>

@endsection