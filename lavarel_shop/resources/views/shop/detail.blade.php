@extends('master')
@section('title', $product->name)

@section('content')
<?php
$list_type = array(
	array('id'=>'1', 'name'=>'Điện thoại'),
	array('id'=>'2', 'name'=>'Máy tính xách tay'),
	array('id'=>'3', 'name'=>'Máy tính bảng'),
	array('id'=>'4', 'name'=>'Phụ kiện')

);
?>


<div class="container" style="margin-top: 50px">
	<div class="row">
		<div class="col-md-12">
			<h3>{{$product->name}}</h3>
			<hr>
		</div>
		<div class="col-md-9">
			
			<div class="col-md-6">
				<img  src=" {{asset($product->image)}}"  height="400px" width="400px">
			</div>
			<div class="col-md-6">
				<table class="detail_table">
					
					<tr>
						<td class="col-md-5"><h5>Loại sản phẩm:</h5></td>
						@foreach($list_type as $type)
						@if($type['id'] == $product->type_id)
						<td class="col-md-7"><h5>{{$type['name']}}</h5></td>
						@endif
						@endforeach
					</tr>
					<tr>
						<td class="col-md-5"><h5>Hãng sản xuất:</h5></td>
						<td class="col-md-7"><h5>{{$trade->name}}</h5></td>
					</tr>
					<tr>
						<td class="col-md-5"><h5>Ngày cập nhật:</h5></td>
						<td class="col-md-7"><h5>{{$product->updated_at}}</h5></td>
					</tr>
					<tr>
						<td class="col-md-5"><h5>Trạng thái:</h5></td>
						@if($product->status == 1)
						<td class="col-md-7"><h5>Còn hàng</h5></td>
						@elseif($product->status == 2)
						<td class="col-md-7"><h5>Hết hàng</h5></td>
						@endif

					</tr>
					<tr>
						<td class="col-md-5"><h5>Giá sản phẩm:</h5></td>
						<td class="col-md-7"><h4 style="color: #da1821">{{$product->price}} VNĐ</h4></td>
					</tr>
					<tr style="border: none;">
						<td colspan="2"><h5 style="color: #da1821">*Tạo tài khoản và đặt hàng ngay để nhận những ưu đãi hấp dẫn từ shop</h5></td>
						
					</tr>

					<tr style="border: none;">
						<td class="col-md-12" colspan="2">
							@if($product->status == 2)
							<a href="#"><span class="btn btn-primary" style="width: 100%" disabled>Đã có tài khoản mua ngay</span></a>
							@else
							<a href="#"><span class="btn btn-primary" style="width: 100%" data-toggle="modal" data-target="#warning">Đã có tài khoản mua ngay</span></a>
							@endif
						</td>
					</tr>
					<tr style="border: none;">
						<td><br></td>
					</tr>
					<tr style="border: none;">
						<td class="col-md-12" colspan="2">
							@if($product->status == 2)
							<a href="#"><span class="btn btn-success" style="width: 100%" disabled>Đăng ký tài khoản và mua ngay</span></a>
							@else
							<a href="#"><span class="btn btn-success" style="width: 100%" data-toggle="modal" data-target="#warning">Đăng ký tài khoản và mua ngay</span></a>
							@endif
						</td>
					</tr>
					<tr style="border: none;">
						<td><br></td>
					</tr>
					<tr style="border: none; ">
						<td class="col-md-12" colspan="2">
							@if($product->status == 2)

							<span class="btn btn-warning" style="width: 100%" data-toggle="modal" data-target="#myModal" disabled>Mua ngay không cần tài khoản</span >

							@else
							<span class="btn btn-warning" style="width: 100%" data-toggle="modal" data-target="#myModal">Mua ngay không cần tài khoản</span>
							@endif

						</td>
					</tr>


				</table>
			</div>


			<div class="description col-md-12" style="margin-top: 50px">
				<div style="color: #da1821">
					<h4>Thông số kỹ thuật</h4>
				</div>
				<table class="table table-bordered" >
					<tr>
						<td class="col-md-3"><h5><center><strong>Hệ điều hành</strong></center></h5></td>
						<td class="col-md-3"><h5><center><strong>{{$des->operating}}</strong></h5> </center></td>
					</tr>
					<tr>
						<td class="col-md-3"><h5><center><strong>Kích thước màn hình</strong></center></h5></td>
						<td class="col-md-3"><h5><center><strong>{{$des->size}}</strong></h5> </center></td>
					</tr>
					<tr>
						<td class="col-md-3"><h5><center><strong>Độ phân giải</strong></center></h5></td>
						<td class="col-md-3"><h5><center><strong>{{$des->resolution}}</strong></h5> </center></td>
					</tr>
					<tr>
						<td class="col-md-3"><h5><center><strong>Chip xử lý(CPU)</strong></center></h5></td>
						<td class="col-md-3"><h5><center><strong>{{$des->cpu}}</strong></h5> </center></td>
					</tr>
					<tr>
						<td class="col-md-3"><h5><center><strong>RAM</strong></center></h5></td>
						<td class="col-md-3"><h5><center><strong>{{$des->ram}}</strong></h5> </center></td>
					</tr>
					<tr>
						<td class="col-md-3"><h5><center><strong>Máy ảnh chính</strong></center></h5></td>
						<td class="col-md-3"><h5><center><strong>{{$des->camera}}</strong></h5> </center></td>
					</tr>
					<tr>
						<td class="col-md-3"><h5><center><strong>Bộ nhớ trong</strong></center></h5></td>
						<td class="col-md-3"><h5><center><strong>{{$des->memory}}</strong></h5> </center></td>
					</tr>
					<tr>
						<td class="col-md-3"><h5><center><strong>Pin</strong></center></h5></td>
						<td class="col-md-3"><h5><center><strong>{{$des->pin}}</strong></h5> </center></td>
					</tr>


				</table>
			</div>
		</div>

		<div class="col-md-3" style="padding: 0px;" >
			<h4><center>Sản phẩm cùng loại mới nhất</center></h4>
			<table class="table table-hover table" style="border: 1px solid #dddddd; ">
				@foreach($list_proType as $product2)
				<tr>
					<td><a href="{{url('/detail',$product2->id)}}"><img src=" {{asset($product2->image)}}" height="50px" width="50px" style="border-radius: 50%;"></a></td>
					<td>
						<div style="height:40px; overflow: hidden;"><a href="{{url('/detail',$product2->id)}}">{{ $product2->name}}</a></div>
						<div style="color: #da1821"><h4>{{ $product2->price}} VNĐ</h4></div>
					</td>
				</tr>
				@endforeach
			</table>
			<hr>

			<h4><center>Sản phẩm cùng hãng mới nhất</center></h4>
			<table class="table table-hover table" style="border: 1px solid #dddddd;">
				@foreach($list_proTrade as $product1)
				<tr>
					<td><a href="{{url('/detail',$product1->id)}}"><img src=" {{asset($product1->image)}}" height="50px" width="50px" style="border-radius: 50%;"></a></td>
					<td>
						<div style="height:40px; overflow: hidden;"><a href="{{url('/detail',$product1->id)}}">{{ $product1->name}}</a></div>
						<div style="color: #da1821"><h4>{{ $product1->price}} VNĐ</h4></div>
					</td>
				</tr>
				@endforeach
			</table>

		</div>

	</div>
</div>


<div class="modal fade" id="myModal" role="dialog">

	<div class="modal-dialog" style="width: 70%">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Đặt hàng {{$product->name}}</h4>
			</div>
			<div class="modal-body">
		
				<form class="form-horizontal" action="{{ url('/order/handle_no_account') }}" enctype="multipart/form-data" method="POST">
					{{ csrf_field()}}
					<div class="form-group">
						<div class="container">
							<div class="col-md-4" style="border-right: 1px solid #dddddd">
								<center><img src=" {{asset($product->image)}}"  height="250" width="250" ></center>
								<h5><center>{{$product->name}}</center></h5>
								<h3  style="color:red"><strong><center>{{$product->price}} VNĐ</center></strong></h3>
							</div>
							<div class="col-md-6">
								<label class="control-label  col-md-9" style="margin-top: -20px; "><h3>Thông tin khách hàng</h3></label>
								<input type="hidden" name="pro_id" value="{{$product->id}}">
								<input type="hidden" name="name_pro" value="{{$product->name}}">
								<input type="hidden" name="price" value="{{$product->price}}">
								<label class="control-label  col-md-4" >Tên khách hàng:</label>
								<div class="col-md-7" style="margin-bottom: 10px; ">
									<input type="text" class="form-control" name="name" placeholder="Nhập tên khách hàng" required >
								</div>
								<label class="control-label  col-md-4" >Số điện thoại:</label>
								<div class="col-md-7" style="margin-bottom: 10px; ">
									<input type="text" class="form-control" name="phone" placeholder="Nhập số điện thoại" required >
								</div>
								<label class="control-label  col-md-4" >Email:</label>
								<div class="col-md-7" style="margin-bottom: 10px; ">
									<input type="text" class="form-control" name="email" placeholder="Nhập email" required >
								</div>
								<label class="control-label  col-md-4" >Địa chỉ:</label>
								<div class="col-md-7" style="margin-bottom: 10px; ">
									<input type="text" class="form-control" name="address" placeholder="Nhập địa chỉ khách hàng" required >
								</div>
									<label class="control-label  col-md-4" >Số lượng muốn mua:</label>
								<div class="col-md-7" style="margin-bottom: 10px; ">
									<input type="number" class="form-control" name="number" value="1" required >
								</div>
									<div class="col-md-offset-4 col-md-7">
									<input type="submit" class="form-control btn btn-primary" name="submit" value="Mua hàng" >
								</div>
							</div>
						</div>

					</div>
				</form>



			</div>
			<div class="modal-footer">

				Hotline bán hàng Online: 19002091 ấn phím 1 (08:30 - 18:00)
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="warning" role="dialog">
	<div class="modal-dialog" >
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Đặt hàng {{$product->name}}</h4>
			</div>
			<div class="modal-body">
				<h1 class="text-danger">Chức năng đang phát triển</h1>
			</div>
			<div class="modal-footer">
				Hotline bán hàng Online: 19002091 ấn phím 1 (08:30 - 18:00)
			</div>
		</div>
	</div>
</div>


</div>
@endsection