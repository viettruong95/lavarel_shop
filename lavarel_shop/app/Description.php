<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Description extends Model
{
      protected $table = 'description';
	protected $fillable = ['id','size','resolution', 'operating', 'cpu', 'ram', 'camera', 'memory', 'pin'];
	public $timestamps = false;

	public function product (){
		 return $this->hasMany('App\Product', 'description');
	}
}
