@if(session::get('type')==1)
@extends('admin_master')
@if($_GET['type'] == 1)
@section('title', 'Quản lý tài khoản admin')
@elseif($_GET['type'] == 2)
@section('title', 'Quản lý tài khoản nhân viên')
@elseif($_GET['type'] == 3)
@section('title', 'Quản lý tài khoản khách')
@endif
@section('content')
@if (isset($error) > 0)
<div class="alert alert-danger">
	{{ $error }}
</div>
@endif
@if (isset($success) > 0)
<div class="alert alert-success">
	{{ $success }}
</div>
@endif
<a href="{{url('admin/acc/add')}}"><button class="btn btn-primary" style="float: right; margin-bottom: 20px" >Thêm mới admin</button></a>
<table class="table table-bordered table-striped" style="text-align: center;">
	<thead >
		<tr >
			<th style="text-align: center;">Mã tài khoản</th>
			<th style="text-align: center;">Ảnh</th>
			<th style="text-align: center;">Tên đăng nhập</th>
			<th style="text-align: center;">Chức danh</th>
			<th style="text-align: center;">Mã thông tin cá nhân</th>
			<th></th>
			<th></th>

		</tr>
	</thead>
	<tbody>
		@foreach ($list_acc as $acc )
		<tr>
			<td>{{$acc->id}}</td>
			<td><img src="/{{$acc->image}}" style="width: 100px; height: 100px"></td>
			<td>{{$acc->user_name}}</td>
			<td>
				
				@if( $acc->type == 1)
				Admin
				@elseif( $acc->type == 2)
				Nhân viên
				@elseif( $acc->type == 3)
				Khách hàng
				@endif
			</td>
			<td>{{$acc->user_id}}</td>

			<td>@if( $_GET['type'] == 1 & $acc->id == session::get('id'))<a href="{{url('admin/acc/edit',$acc->id)}}">Sửa</a>@endif</td>
			<td>@if( $acc->id != session::get('id'))<a href="{{url('admin/handle_delete_acc',$acc->id)}}" onclick="return confirm('Bạn có chắc muốn xóa sản phẩm này ko')">Xóa</a>@endif</td>
		
		</tr>
		
		@endforeach

	</tbody>
</table>
{!! $list_acc->links() !!}
@endsection
@endif