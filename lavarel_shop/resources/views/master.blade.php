<!DOCTYPE html>
<html>
<head>
	<title>MasterShop - @yield('title')</title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc= sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
	<link rel="stylesheet"  href="{{URL::asset('css/style.css')}}">
</head>
<body>
	<header class="container-fluid">
		<div class="row">
			<nav class="navbar">
				<div class="container-fluid">
					<div class="navbar-header">
						<a class="navbar-brand" href="{{url('/')}}"">Master Shop</a>
					</div>
					<ul class="nav navbar-nav">
						<li><a href="#">Điện thoại</a></li>
						<li><a href="#">Máy tính sách tay</a></li>
						<li><a href="#">Máy tính bảng</a></li>
						<li><a href="#">Phụ kiện</a></li>
					</ul>
					<form class="navbar-form navbar-left" action="/action_page.php">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="Master tìm kiếm">
						</div>
						<button type="submit" class="btn btn-default">Tìm kiếm</button>
					</form>
					
					
				</div> 
			</nav>
		</div>
		
	</header>
	<div>
		 @yield('content')
	</div>
	<footer style="background-color: #1e597d; margin-top:20px; color: white" >
		<div class="container-fluid text-center">
			<hr />
			<div class="row">
				<div id="footer">
				<div class="col-xs-12 col-sm-3 col-md-3">
					<h4>Quick links</h4>
					<ul class="list-unstyled quick-links">
						<li><a href="#">Home</a></li>
						<li><a href="#">About</a></li>
						<li><a href="#">FAQ</a></li>
						<li><a href="#">Get Started</a></li>
						<li><a href="#"></i>Videos</a></li>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-3 col-md-3">
					<h4>Quick links</h4>
					<ul class="list-unstyled quick-links">
						<li><a href="#">Home</a></li>
						<li><a href="#">About</a></li>
						<li><a href="#">FAQ</a></li>
						<li><a href="#">Get Started</a></li>
						<li><a href="#"></i>Videos</a></li>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-3 col-md-3">
					<h4>Quick links</h4>
					<ul class="list-unstyled quick-links">
						<li><a href="#">Home</a></li>
						<li><a href="#">About</a></li>
						<li><a href="#">FAQ</a></li>
						<li><a href="#">Get Started</a></li>
						<li><a href="#"></i>Videos</a></li>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-3 col-md-3">
					<h4>Quick links</h4>
					<ul class="list-unstyled quick-links">
						<li><a href="#">Home</a></li>
						<li><a href="#">About</a></li>
						<li><a href="#">FAQ</a></li>
						<li><a href="#">Get Started</a></li>
						<li><a href="#"></i>Videos</a></li>
					</ul>
				</div>
			</div>	
		</div>
			
			<HR/>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
					<p>National Transaction Corporation</a> is a Registered MSP/ISO of Elavon, Inc. Georgia [a wholly owned subsidiary of U.S. Bancorp, Minneapolis, MN]</p>
					<p class="h6">&copy All right Reversed.Sunlimetech</p>
				</div>
			</hr>
		</div>	
	</div>
</footer>
</body>