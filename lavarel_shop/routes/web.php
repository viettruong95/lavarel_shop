<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web']],function(){

	Route::get('/admin/login','AdminController@login');
	Route::post('/admin/handle_login','AdminController@handle_login');
	Route::get('/admin/handle_logout','AdminController@handle_logout');
	Route::get('/admin/home', 'AdminController@home');
	Route::get('/admin', 'AdminController@home');
	Route::get('/admin/product', 'AdminController@list_product');
	Route::get('/admin/product/add', 'AdminController@add_product');
	Route::get('/admin/product/edit/{id}', 'AdminController@edit_product');
	Route::post('/admin/handle_edit_pro', 'AdminController@handle_edit_pro');
	Route::post('/admin/handle_add_pro', 'AdminController@handle_add_pro');
	Route::get('/admin/handle_delete_pro/{id_pro}', 'AdminController@handle_delete_pro');
	Route::get('/admin/system/trade', 'AdminController@list_trade');
	Route::post('/admin/system/trade', 'AdminController@trade_handling');
	Route::get('/admin/system/color', 'AdminController@list_color');
	Route::post('/admin/system/color', 'AdminController@color_handling');
	Route::get('/admin/acc', 'AdminController@list_acc');
	Route::get('/admin/acc/add', 'AdminController@add_acc');
	Route::get('/admin/acc/edit/{id}', 'AdminController@edit_acc');
	Route::post('/admin/handle_edit_acc', 'AdminController@handle_edit_acc');
	Route::post('/admin/handle_add_acc', 'AdminController@handle_add_acc');
	Route::get('/admin/handle_delete_acc/{id}', 'AdminController@handle_delete_acc');
	Route::get('/admin/order/list_order', 'AdminController@list_order');
	Route::post('/admin/order/handle_order', 'AdminController@handle_order');
	Route::get('/admin/order/edit/{id}', 'AdminController@edit_order');
	Route::get('/', 'ShopController@home');
	Route::get('/detail/{id}', 'ShopController@detail');
	Route::post('/order/handle_no_account', 'ShopController@handle_no_account');
	





});