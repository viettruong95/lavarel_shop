<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use App\User;
use App\Account;
use App\Product;
use App\Trade;
use App\Image;
use App\Color;
use App\Order;
use App\Order_Detail;
use App\Pro_color;
//  array('id'=>'1', 'name'=>'Điện thoại'),
// 	array('id'=>'2', 'name'=>'Máy tính xách tay'),
// 	array('id'=>'3', 'name'=>'Máy tính bảng'),
// 	array('id'=>'4', 'name'=>'Phụ kiện')

class ShopController extends Controller
{
	public function home(){
		$list_phone=Product::where('type_id', '1')->orderBy('id', 'desc')->paginate(10);
		$list_laptop=Product::where('type_id', '2')->orderBy('id', 'desc')->paginate(10);
		$list_tablet=Product::where('type_id', '3')->orderBy('id', 'desc')->paginate(10);
		$list_sub=Product::where('type_id', '4')->orderBy('id', 'desc')->paginate(10);
		$list_trade=Trade::all();
		return view('shop/home', ['list_phone' => $list_phone	, 'list_laptop' => $list_laptop, 'list_sub' =>$list_sub,'list_trade'=>$list_trade, 'list_tablet' => $list_tablet]);
	}
	public function detail($id){
		$product = Product::find($id);
		$list_proType = Product::where('type_id', $product->type_id)->orderBy('id', 'desc')->paginate(4);
		$list_proTrade = Product::where('trade_id', $product->trade_id)->orderBy('id', 'desc')->paginate(4);
		$trade = Product::find($id)->trade()->first();
		$des = Product::find($id)->description()->first();

		return view('shop.detail', ['product'=>$product, 'list_proType' => $list_proType, 'list_proTrade' => $list_proTrade, 'trade' => $trade, 'des' => $des]);
	}
	public function handle_no_account(Request $request){
		$user = new User;
		$user->name = $request->name;
		$user->phone = $request->phone;
		$user->email = $request->email;
		$user->type = 3;
		$user->address = $request->address;
		$order_detail = new Order_Detail;
		$order_detail->name_pro = $request->name_pro;
		$order_detail->number = $request->number;
		$order_detail->price = $request->price;
		$order_detail->product_id = $request->pro_id;

		$order = new Order;
		$order->email_cus = $request->email;
		$order->name_cus = $request->name;
		$order->phone_cus = $request->phone;
		$order->address_cus = $request->address;
		$order->total_price = ($request->number)*($request->price);
		$order->status = 1;

		$user->save();
		$order->id_cus = $user->id;
		$order->save();
		$order_detail->order_id = $order->id;
		$order_detail->save();
		
		return redirect('/');
	}
}
